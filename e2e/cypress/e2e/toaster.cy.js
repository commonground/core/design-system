// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
/// <reference types="cypress" />
import { checkStory, currentComponent } from '../support'

describe('Toaster', () => {
  ;['default', 'with-delay'].forEach((story) => {
    it.skip(`story '${story}' should be accessible`, () => {
      checkStory(currentComponent(), story)
    })
  })
})
