// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
/// <reference types="cypress" />
import { checkStory, currentComponent } from '../support'

describe('Form', () => {
  ;['intro', 'label', 'field-label', 'fieldset-with-legend'].forEach(
    (story) => {
      it(`story '${story}' should be accessible`, () => {
        checkStory(currentComponent(), story)
      })
    },
  )

  describe('Checkbox', () => {
    ;['intro', 'disabled'].forEach((story) => {
      it(`story '${story}' should be accessible`, () => {
        checkStory('form-checkbox', story)
      })
    })
  })

  describe('ErrorMessage', () => {
    ;['intro'].forEach((story) => {
      it(`story '${story}' should be accessible`, () => {
        checkStory('form-errormessage', story)
      })
    })
  })

  describe('Radio', () => {
    ;['intro', 'disabled'].forEach((story) => {
      it(`story '${story}' should be accessible`, () => {
        checkStory('form-radio', story)
      })
    })
  })

  describe('Select', () => {
    ;[
      'intro',
      'validation',
      'size',
      'disabled',
      'multi',
      'objects-as-values',
      'multi-object-as-value',
      'select-formik',
      'select-component',
    ].forEach((story) => {
      it(`story '${story}' should be accessible`, () => {
        checkStory('form-select', story)
      })
    })
  })

  describe('TextInput', () => {
    ;[
      'intro',
      'placeholder',
      'show-error',
      'size',
      'disabled',
      'date-type',
      'with-icon',
      'with-input-clear',
      'textarea-type',
      'input-component',
    ].forEach((story) => {
      it(`story '${story}' should be accessible`, () => {
        checkStory('form-textinput', story)
      })
    })
  })
})
