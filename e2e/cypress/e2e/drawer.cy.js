// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
/// <reference types="cypress" />
import { checkStory, currentComponent } from '../support'

describe('Drawer', () => {
  ;[
    'default',
    'focus',
    'drawer-without-mask',
    'stacked-drawers',
    'long-title',
  ].forEach((story) => {
    it(`story '${story}' should be accessible`, () => {
      checkStory(currentComponent(), story)
    })
  })
})
