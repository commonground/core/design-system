// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
/// <reference types="cypress" />
import { checkStory, currentComponent } from '../support'

describe('Button', () => {
  ;['all-buttons', 'with-icons', 'as-other-component'].forEach((story) => {
    it(`story '${story}' should be accessible`, () => {
      checkStory(currentComponent(), story)
    })
  })
})
