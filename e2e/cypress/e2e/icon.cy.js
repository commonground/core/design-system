// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
/// <reference types="cypress" />
import { checkStory, currentComponent } from '../support'

describe('Icon', () => {
  ;['default', 'inline-icon', 'icon-sizes'].forEach((story) => {
    it(`story '${story}' should be accessible`, () => {
      checkStory(currentComponent(), story)
    })
  })
})
