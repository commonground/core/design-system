// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
/// <reference types="cypress" />
import { checkStory, currentComponent } from '../support'

describe('NLXLogo', () => {
  ;['the-logo', 'on-dark'].forEach((story) => {
    it(`story '${story}' should be accessible`, () => {
      checkStory(currentComponent(), story)
    })
  })
})
