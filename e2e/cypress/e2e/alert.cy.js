// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
/// <reference types="cypress" />
import { checkStory, currentComponent } from '../support'

describe('Alert', () => {
  ;['info', 'success', 'warning', 'error', 'with-actions'].forEach((story) => {
    it(`story '${story}' should be accessible`, () => {
      checkStory(currentComponent(), story)
    })
  })
})
