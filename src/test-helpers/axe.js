// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import axe from 'axe-core'
import chalk from 'chalk'
import { printReceived, matcherHint } from 'jest-matcher-utils'

// Based on: https://github.com/nickcolley/jest-axe/blob/9b1b83be22339cb7434b1a29228636a347037771/index.js#L116
const expectExtensions = {
  toHaveNoA11yViolations(results) {
    const lineBreak = '\n\n'
    const horizontalLine = '\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500'

    const reporter = (violations) => {
      return violations
        .map((violation) => {
          const errorBody = violation.nodes
            .map((node) => {
              const selector = node.target.join(', ')
              const expectedText =
                `Expected the HTML found at $('${selector}') to have no violations:` +
                lineBreak
              return (
                expectedText +
                chalk.grey(node.html) +
                lineBreak +
                `Received:` +
                lineBreak +
                printReceived(`${violation.help} (${violation.id})`) +
                lineBreak +
                chalk.yellow(node.failureSummary) +
                lineBreak +
                (violation.helpUrl
                  ? `You can find more information on this issue here: \n${chalk.blue(
                      violation.helpUrl,
                    )}`
                  : '')
              )
            })
            .join(lineBreak)

          return errorBody
        })
        .join(lineBreak + horizontalLine + lineBreak)
    }

    const violations = results.violations

    const pass = violations.length === 0
    const message = () => {
      if (pass) {
        return
      }
      const message = reporter(violations)

      return matcherHint('.toHaveNoA11yViolations') + lineBreak + message
    }

    return { pass: pass, message: message }
  },
}

const setupAxe = () => {
  axe.configure({
    reporter: 'v1',
  })

  expect.extend(expectExtensions)
}

const axeRunOptions = {
  runOnly: ['best-practice', 'wcag2a', 'wcag21a'],
}

const runAxe = (element) => {
  return new Promise((resolve, reject) => {
    axe.run(element, axeRunOptions, (err, results) => {
      if (err) {
        reject(err)
      }
      resolve(results)
    })
  })
}

export { setupAxe, runAxe }
