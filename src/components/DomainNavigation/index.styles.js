// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled, { css } from 'styled-components'
import Icon from '../Icon'
import mediaQueries from '../../mediaQueries'

const navWrapperHeight = '48px'

const dividerStyle = css`
  content: '';
  height: 24px;
  left: 0;
  position: absolute;
  width: 100%;
`

export const Navigation = styled.nav`
  box-sizing: content-box;
  display: flex;
  font-family: 'Source Sans Pro', sans-serif;
  height: ${navWrapperHeight};
  justify-content: center;
  line-height: ${(p) => p.theme.tokens.lineHeightTextSmall};
  position: relative;

  ${mediaQueries.lgUp`
    border-bottom: solid 1px;
    border-bottom-color: ${(p) => p.theme.colorDividerSecondary};
  `}

  > * {
    align-items: center;
    display: flex;
    font-size: 14px;
    height: 100%;
    padding: 0 ${(p) => p.theme.tokens.spacing05} 0;
    position: relative;

    :first-child {
      color: ${(p) => p.theme.colorTextLabel};
    }

    ${mediaQueries.lgUp`
      padding: 0 ${(p) => p.theme.tokens.spacing06};
    `}
  }
`

export const IntroText = styled.p`
  margin: 0;

  ${mediaQueries.lgUp`
    > br {
      display: none;
    }
  `}
`

export const DropdownButton = styled.div`
  color: ${(p) => p.theme.colorText};
  cursor: pointer;
  font-size: 14px;
  user-select: none;
  vertical-align: middle;
  white-space: nowrap;

  :before {
    ${dividerStyle}
    border-left: solid 1px;
    border-left-color: ${(p) => p.theme.colorDividerPrimary};
  }

  > svg {
    margin-left: ${(p) => p.theme.tokens.spacing02};
  }

  ${mediaQueries.lgUp`
    display: none;
  `}
`

export const DropdownList = styled.ul`
  background: ${(p) => p.theme.colorBackgroundDropdown};
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
  display: flex;
  flex-direction: column;
  height: fit-content;
  margin: 0;
  margin-top: ${(p) => p.theme.tokens.spacing02};
  top: ${navWrapperHeight};
  bottom: 0;
  padding: 0;
  position: absolute;
  width: ${(p) => `calc(100vw - ${p.theme.tokens.spacing05})`};
  z-index: 15;

  ${mediaQueries.lgUp`
    display: none;
  `}
`

export const Label = styled.p`
  margin: 0;

  > span {
    color: ${(p) =>
      p.isActive ? p.theme.colorTextDropdownActive : p.theme.colorTextLabel};
    display: block;
    font-size: 14px;
    line-height: 1.5;
    margin-top: ${(p) => p.theme.tokens.spacing02};
  }
`

export const ExternalSiteIcon = styled(Icon)`
  flex-shrink: 0;
  height: 16px;
  margin: auto 0 auto ${(p) => p.theme.tokens.spacing03};
`

export const ListItem = styled.li`
  background: ${(p) => p.isSelected && p.theme.colorBackgroundDropdownSelected};
  list-style: none;
  width: 100%;

  :hover {
    background: ${(p) => !p.isSelected && p.theme.colorBackgroundDropdownHover};
  }

  > * {
    color: inherit;
    display: flex;
    justify-content: space-between;
    margin: 0;
    padding: ${(p) =>
      `${p.theme.tokens.spacing04} ${p.theme.tokens.spacing05}`};
    text-decoration: inherit;

    :hover {
      color: inherit;
    }
  }

  ${ExternalSiteIcon} {
    fill: ${(p) => p.theme.colorTextLabel};
  }

  :hover {
    ${ExternalSiteIcon} {
      fill: ${(p) => p.theme.colorTextDropdownActive};
    }

    ${Label} {
      > span {
        color: ${(p) => !p.isSelected && p.theme.colorTextDropdownActive};
      }
    }
  }
`

export const DomainListDesktop = styled.div`
  display: none;

  ${mediaQueries.lgUp`
    display: flex;

    :before {
      ${dividerStyle}
      border-left: solid 1px;
      border-left-color: ${(p) => p.theme.colorDividerPrimary};
    }
  `}
`

export const ListItemDesktop = styled.a`
  align-items: center;
  color: inherit;
  display: flex;
  font-weight: ${(p) => p.isSelected && 700};
  height: 100%;
  justify-content: center;
  position: relative;
  text-decoration: inherit;
  white-space: nowrap;

  span:first-child {
    position: absolute;
  }
  span:last-child {
    font-weight: 700;
    opacity: 0;
  }

  :hover {
    color: inherit;

    span:first-child {
      font-weight: 700;
      opacity: 1;
    }
  }

  + a {
    margin-left: ${(p) => p.theme.tokens.spacing07};
  }

  ${(p) =>
    p.isSelected &&
    `
      :before {
        border-bottom: solid 1px;
        border-bottom-color: ${(p) => p.theme.colorText}
        bottom: -1px;
        content: '';
        height: 100%;
        position: absolute;
        width: 100%;
      }
  `}
`

export const GitLabIcon = styled(Icon)`
  height: 16px;
  fill: ${(p) => p.theme.colorText};
`

export const GitLabLink = styled.a`
  color: ${(p) => p.theme.colorText};
  position: relative;

  :before {
    ${dividerStyle}
    border-left: solid 1px;
    border-left-color: ${(p) => p.theme.colorDividerPrimary};
  }

  :hover {
    color: inherit;
  }

  span {
    display: none;
  }

  ${mediaQueries.lgUp`
    text-decoration: none;

    > div {
      display: flex;
      justify-content: center;
    }

    span {
      display: block;
      margin-left: ${(p) => p.theme.tokens.spacing03};
    }

    span:first-child {
      position: absolute;
    }
    span:last-child {
      font-weight: 700;
      opacity: 0;
    }

    :hover {
      span:first-child {
        font-weight: 700;
        opacity: 1;
      }
    }
  `}
`
