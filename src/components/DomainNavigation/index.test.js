// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import TestThemeProvider from '../../themes/TestThemeProvider'
import DomainNavigation from './index'

test('shows domain navigation after dropdown is selected', async () => {
  const { queryByRole, getByText } = render(
    <TestThemeProvider>
      <DomainNavigation activeDomain="NLX" dropdownLabel="Dropdown Label" />
    </TestThemeProvider>,
  )
  expect(queryByRole('list')).toBeNull()
  fireEvent.click(getByText('Dropdown Label'))
  expect(queryByRole('list')).toBeTruthy()
})
