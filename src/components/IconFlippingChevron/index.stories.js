// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState } from 'react'
import IconFlippingChevron from './index'

const iconFlippingChevronStory = {
  title: 'Components/IconFlippingChevron',
  parameters: {
    componentSubtitle: 'Chevron icon which can be flipped using animations.',
  },
  component: IconFlippingChevron,
}

export default iconFlippingChevronStory

export const theLogo = () => <IconFlippingChevron />

export const FlipHorizontal = () => <IconFlippingChevron flipHorizontal />

export const AnimationDuration = () => {
  const [flipped, setFlipped] = useState(false)
  const [animationDuration, setAnimationDuration] = useState(150)
  return (
    <>
      <IconFlippingChevron
        flipHorizontal={flipped}
        animationDuration={animationDuration}
      />
      <hr />
      <label>
        Animation duration (in milliseconds){' '}
        <input
          value={animationDuration}
          onChange={(e) => setAnimationDuration(e.target.value)}
        />
      </label>
      <br />
      <button onClick={() => setFlipped(!flipped)}>Flip!</button>
    </>
  )
}
