// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import selectEvent from 'react-select-event'
import { Formik, Form } from 'formik'
import TestThemeProvider from '../../../themes/TestThemeProvider'
import { Select } from './index'

test('Select should accept strings as values', async () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const initialValues = { selection: [options[0].value] }

  const { getByTestId, getByRole } = render(
    <TestThemeProvider>
      <Formik initialValues={initialValues} onSubmit={() => {}}>
        {(formikProps) => {
          const selectionAsText = formikProps.values.selection
          return (
            <Form>
              <Select options={options} name="selection" />
              <span data-testid="parsed-selection">{selectionAsText}</span>
            </Form>
          )
        }}
      </Formik>
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('one')

  await selectEvent.select(getByRole('combobox'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('two')
})

test('Select should accept object as values', async () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const initialValues = { selection: [options[0].value] }

  const { getByTestId, getByRole } = render(
    <TestThemeProvider>
      <Formik initialValues={initialValues} onSubmit={() => {}}>
        {(formikProps) => {
          const selectionAsText = formikProps.values.selection
          return (
            <Form>
              <Select options={options} name="selection" />
              <span data-testid="parsed-selection">{selectionAsText}</span>
            </Form>
          )
        }}
      </Formik>
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('one')

  await selectEvent.select(getByRole('combobox'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('two')
})

test('Select should accept strings as values while accepting multiple values', async () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const initialValues = { selection: [options[0].value] }

  const { getByTestId, getByRole } = render(
    <TestThemeProvider>
      <Formik initialValues={initialValues} onSubmit={() => {}}>
        {(formikProps) => {
          const selectionAsText = formikProps.values.selection.join(',')
          return (
            <Form>
              <Select options={options} name="selection" isMulti />
              <span data-testid="parsed-selection">{selectionAsText}</span>
            </Form>
          )
        }}
      </Formik>
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('one')

  await selectEvent.select(getByRole('combobox'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('one,two')
})

test('Select should accept objects as values while accepting multiple values', async () => {
  const options = [
    { value: { english: 'one' }, label: 'Uno' },
    { value: { english: 'two' }, label: 'Dos' },
    { value: { english: 'three' }, label: 'Tres' },
  ]

  const initialValues = { selection: [options[0].value] }

  const { getByTestId, getByRole } = render(
    <TestThemeProvider>
      <Formik initialValues={initialValues} onSubmit={() => {}}>
        {(formikProps) => {
          const selectionAsText = formikProps.values.selection
            .map((select) => select.english)
            .join(',')

          return (
            <Form>
              <Select options={options} name="selection" isMulti />
              <span data-testid="parsed-selection">{selectionAsText}</span>
            </Form>
          )
        }}
      </Formik>
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('one')

  await selectEvent.select(getByRole('combobox'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('one,two')
})
