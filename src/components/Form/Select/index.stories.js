// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { Form, Formik, Field } from 'formik'
import { Select, SelectFormik, SelectComponent } from './index'

const selectStory = {
  title: 'Components/Form/Select',
  parameters: {
    componentSubtitle: 'Select component.',
  },
  component: Select,
}

export default selectStory

export const intro = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  return (
    <Formik initialValues={{}} onSubmit={() => {}}>
      <Form>
        <Select options={options} name="option">
          Select option
        </Select>
      </Form>
    </Formik>
  )
}

export const validation = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const validation = (values) => {
    const errors = {}
    if (!values.option || values.option !== 'one') {
      errors.option = 'Option one is the only valid choice.'
    }
    return errors
  }

  return (
    <Formik initialValues={{}} onSubmit={() => {}} validate={validation}>
      <Form>
        <Select options={options} name="option">
          Select option
        </Select>
      </Form>
    </Formik>
  )
}

export const size = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  return (
    <Formik initialValues={{}} onSubmit={() => {}}>
      <Form>
        <Select options={options} name="xs" size="xs">
          Extra small
        </Select>

        <Select options={options} name="s" size="s">
          Small
        </Select>

        <Select options={options} name="m" size="m">
          Medium
        </Select>

        <Select options={options} name="l" size="l">
          Large
        </Select>

        <Select options={options} name="xl" size="xl">
          Extra large
        </Select>
      </Form>
    </Formik>
  )
}

export const disabled = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  return (
    <Formik initialValues={{}} onSubmit={() => {}}>
      <Form>
        <Select options={options} name="option" disabled>
          Disabled select
        </Select>
      </Form>
    </Formik>
  )
}

export const disabledOption = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres', isDisabled: true },
  ]

  return (
    <Formik initialValues={{}} onSubmit={() => {}}>
      <Form>
        <Select options={options} name="option">
          Disabled select
        </Select>
      </Form>
    </Formik>
  )
}

export const multi = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const initialValues = { option: [options[0].value] }

  return (
    <Formik initialValues={initialValues} onSubmit={() => {}}>
      <Form>
        <Select options={options} name="option" isMulti />
      </Form>
    </Formik>
  )
}

export const multiWithDisabledOption = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres', isDisabled: true },
  ]

  const initialValues = { option: [options[0].value] }

  return (
    <Formik initialValues={initialValues} onSubmit={() => {}}>
      <Form>
        <Select options={options} name="option" isMulti />
      </Form>
    </Formik>
  )
}

export const objectsAsValues = () => {
  const options = [
    { value: { english: 'one' }, label: 'Uno' },
    { value: { english: 'two' }, label: 'Dos' },
    { value: { english: 'three' }, label: 'Tres' },
  ]

  return (
    <Formik initialValues={{ option: options[1].value }} onSubmit={() => {}}>
      <Form>
        <Select options={options} name="option" />
      </Form>
    </Formik>
  )
}

export const multiObjectAsValue = () => {
  const options = [
    { value: { english: 'one' }, label: 'Uno' },
    { value: { english: 'two' }, label: 'Dos' },
    { value: { english: 'three' }, label: 'Tres' },
  ]

  return (
    <Formik initialValues={{ option: [options[1].value] }} onSubmit={() => {}}>
      <Form>
        <Select options={options} name="option" isMulti />
      </Form>
    </Formik>
  )
}

export const selectFormik = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  return (
    <Formik initialValues={{}}>
      <Form>
        <p>
          Using `SelectFormik` directly, which allows you to use the component
          without a label and feedback.
        </p>
        <label>
          Label
          <Field
            component={SelectFormik}
            id="option"
            name="option"
            placeholder="Selecteer optie"
            options={options}
          />
        </label>
      </Form>
    </Formik>
  )
}

export const selectComponent = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  return (
    <form>
      <p>
        Not using Formik? <code>SelectComponent</code> gives you access to the
        dropdown without Formik integration. The React Select package is used as
        foundation. Check out{' '}
        <a
          href="https://react-select.com/props"
          target="_blank"
          rel="noreferrer"
        >
          their website
        </a>{' '}
        to see what additional props you can pass.
      </p>
      <label>
        Label
        <SelectComponent
          id="option"
          name="option"
          placeholder="Selecteer optie"
          options={options}
        />
      </label>
    </form>
  )
}
