// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { forwardRef } from 'react'
import { arrayOf, object, shape, string, bool, oneOfType } from 'prop-types'
import SelectComponent from './../SelectComponent'

/* eslint-disable react/prop-types */
/* prop-types not supported for forwardRef components */
export const SelectComponentWithRef = ({ field, form, ...props }, ref) => {
  const { name } = field
  const { setFieldValue, setTouched } = form
  const { options, isMulti, onChange } = props
  const formikRelated = {}

  // Integrate Formik touched state
  formikRelated.onBlur = () => {
    setTouched({ [name]: true })
  }

  // Extract chosen value and pass to Formik
  formikRelated.onChange = (selectedOption, actionType) => {
    const getOptionValue =
      props.getOptionValue ||
      function (option) {
        return option.value
      }

    if (selectedOption === null) {
      setFieldValue(name, isMulti ? [] : '')
    } else {
      setFieldValue(
        name,
        isMulti
          ? selectedOption.map((option) => getOptionValue(option))
          : getOptionValue(selectedOption),
      )
    }

    if (onChange) {
      onChange(selectedOption, actionType)
    }
  }

  // Loop back the selection to react-select using it's value stored in formik
  formikRelated.value = (() => {
    if (options) {
      return isMulti
        ? options.filter((option) =>
            Array.isArray(field.value)
              ? field.value.some(
                  (value) =>
                    JSON.stringify(value) === JSON.stringify(option.value),
                )
              : JSON.stringify(field.value) === JSON.stringify(option.value),
          )
        : options.find(
            (option) =>
              JSON.stringify(option.value) === JSON.stringify(field.value),
          )
    } else {
      return isMulti ? [] : ''
    }
  })()

  return <SelectComponent ref={ref} {...props} {...formikRelated} />
}
/* eslint-enable react/prop-types */

const SelectFormik = forwardRef(SelectComponentWithRef)

SelectFormik.propTypes = {
  field: object.isRequired,
  form: object.isRequired,
  options: arrayOf(
    shape({
      value: oneOfType([string, object]),
      label: string,
    }),
  ).isRequired,
  isMulti: bool,
}

export default SelectFormik
