// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled, { css } from 'styled-components'

const getSvg = (fillColor) =>
  `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="${fillColor}"><circle cx="8" cy="8" r="4" /></svg>`

export const StyledInput = styled.input`
  outline: none;
  cursor: ${(p) => (p.disabled ? 'auto' : 'pointer')};
  appearance: none;
  width: 1rem;
  height: 1rem;
  border-radius: 1rem;
  vertical-align: middle;

  border: 1px solid
    ${(p) =>
      p.disabled
        ? p.theme.colorBackgroundChoiceDisabled
        : p.theme.colorBorderChoice};

  :focus {
    border: 2px solid ${(p) => p.theme.colorBorderChoiceFocus};
  }

  :checked {
    ${(p) =>
      p.disabled
        ? css`
            background-color: ${(p) => p.theme.tokens.colorBackground};
          `
        : null}

    ${(p) => css`
      background-image: url(data:image/svg+xml;base64,${btoa(
        getSvg(
          p.disabled
            ? p.theme.colorBackgroundChoiceDisabled
            : p.theme.colorBackgroundChoiceSelected,
        ),
      )});
    `}
  }

  + label {
    margin-left: ${(p) => p.theme.tokens.spacing04};
    vertical-align: middle;
    ${(p) =>
      p.disabled
        ? css`
            color: ${(p) => p.theme.colorTextInputLabelDisabled};
          `
        : null}
  }
`
