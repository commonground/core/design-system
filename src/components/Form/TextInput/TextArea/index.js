// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { bool, oneOf } from 'prop-types'
import { StyledTextArea } from './index.styles'

const TextArea = React.forwardRef((props, ref) => (
  <StyledTextArea ref={ref} {...props} />
))

TextArea.propTypes = {
  size: oneOf(['xs', 's', 'm', 'l', 'xl', 'fullWidth']),
  disabled: bool,
}

TextArea.defaultProps = {
  size: 'm',
  disabled: false,
}

export default TextArea
