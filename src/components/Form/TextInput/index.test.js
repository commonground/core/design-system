// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { render, act, fireEvent } from '@testing-library/react'
import * as Yup from 'yup'
import { Formik } from 'formik'
import TestThemeProvider from '../../../themes/TestThemeProvider'
import TextInput from './index'

test('feedback when touching the input element', async () => {
  const validationSchema = Yup.object().shape({
    name: Yup.string().required('Required'),
  })

  const { getByTestId, getByLabelText, getByText } = render(
    <TestThemeProvider>
      <Formik
        initialValues={{ name: '' }}
        validationSchema={validationSchema}
        onSubmit={() => {}}
      >
        <form data-testid="form">
          <TextInput name="name">Name</TextInput>
        </form>
      </Formik>
    </TestThemeProvider>,
  )

  // default UI
  expect(getByText('Name')).toBeTruthy()
  expect(getByLabelText('Name')).toBeTruthy()
  expect(() => getByTestId('error-name')).toThrow()

  // fill-in field with invalid value
  await act(async () => {
    fireEvent.blur(getByLabelText('Name'))
  })

  // assert the conditional fields show validation feedback
  expect(getByTestId('error-name')).toHaveTextContent('Required')

  // fill-in required fields
  await act(async () => {
    fireEvent.change(getByLabelText('Name'), {
      target: { value: 'My name' },
    })
  })

  // error message is hidden
  expect(() => getByTestId('error-name')).toThrow()
})

test('feedback when submitting the form', async () => {
  const onSubmitHandlerSpy = jest.fn()

  const validationSchema = Yup.object().shape({
    name: Yup.string().required('Required'),
  })

  const { getByTestId, getByLabelText, getByText, queryByTestId } = render(
    <TestThemeProvider>
      <Formik
        initialValues={{ name: '' }}
        validationSchema={validationSchema}
        onSubmit={(values) => onSubmitHandlerSpy(values)}
      >
        {({ handleSubmit }) => (
          <form data-testid="form" onSubmit={handleSubmit}>
            <TextInput name="name">Name</TextInput>
          </form>
        )}
      </Formik>
    </TestThemeProvider>,
  )

  // default UI
  expect(getByText('Name')).toBeTruthy()
  expect(getByLabelText('Name')).toBeTruthy()
  expect(queryByTestId('error-name')).not.toBeInTheDocument()

  // submit invalid form
  await act(async () => {
    fireEvent.submit(getByTestId('form'))
  })

  // assert the conditional fields show validation feedback
  expect(getByTestId('error-name')).toHaveTextContent('Required')

  expect(onSubmitHandlerSpy).not.toHaveBeenCalled()

  // fill-in required fields
  await act(async () => {
    fireEvent.change(getByLabelText('Name'), {
      target: { value: 'My name' },
    })
  })

  // error message is hidden
  expect(() => getByTestId('error-name')).toThrow()

  // submit valid form
  await act(async () => {
    fireEvent.submit(getByTestId('form'))
  })

  expect(onSubmitHandlerSpy).toHaveBeenCalledWith({
    name: 'My name',
  })
})
