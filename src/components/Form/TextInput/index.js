// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { bool, oneOf, oneOfType, string, shape, func } from 'prop-types'
import { useField } from 'formik'
import ErrorMessage from '../ErrorMessage'
import { FieldLabel } from '../index'
import { StyledLabel, StyledField } from './index.styles'
import Input from './Input'
import TextArea from './TextArea'

const TextInput = ({ type, children, showError, ...props }) => {
  const [field, meta] = useField(props)

  const hasError =
    typeof showError === 'undefined' ? meta.error && meta.touched : showError

  const { disabled } = props

  return (
    <>
      <StyledLabel disabled={disabled} size={props.size}>
        {children}
        <StyledField
          type={type}
          as={type === 'textarea' ? TextArea : Input}
          {...field}
          value={field.value || ''}
          {...props}
          className={hasError ? 'invalid' : null}
        />
      </StyledLabel>
      {hasError ? (
        <ErrorMessage data-testid={`error-${field.name}`}>
          {meta.error}
        </ErrorMessage>
      ) : null}
    </>
  )
}

TextInput.propTypes = {
  children: oneOfType([string, shape({ type: oneOf([FieldLabel]) })])
    .isRequired,
  disabled: bool,
  handleIconAtEnd: func,
  name: string.isRequired,
  showError: bool,
  size: oneOf(['xs', 's', 'm', 'l', 'xl', 'fullWidth']),
  type: oneOf([
    'text',
    'textarea',
    'number',
    'email',
    'password',
    'tel',
    'url',
    'date',
  ]),
}

TextInput.defaultProps = {
  disabled: false,
  size: 'm',
  type: 'text',
}

export default TextInput
