// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled, { css } from 'styled-components'
import { Label, Fieldset } from '../index'

export const StyledLabel = styled(Label)`
  ${(props) =>
    props.disabled
      ? css`
          cursor: auto;
          color: ${props.theme.colorTextInputLabelDisabled};
          cursor: not-allowed;
        `
      : css`
          cursor: pointer;
        `}

  width: ${({ size }) => (size === 'fullWidth' ? '100%' : '')}
`

export const StyledField = styled(Fieldset)`
  ${(props) =>
    props.type === 'date' &&
    !props.value.length &&
    css`
      ::-webkit-datetime-edit-month-field,
      ::-webkit-datetime-edit-day-field,
      ::-webkit-datetime-edit-year-field {
        opacity: 0.4;
      }
    `}
`
