// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { render } from '@testing-library/react'
import { Formik } from 'formik'
import TestThemeProvider from '../../../themes/TestThemeProvider'
import Checkbox from './index'

test('default ui', async () => {
  const { getByLabelText, getByText } = render(
    <TestThemeProvider>
      <Formik initialValues={{ myCheckbox: '' }} onSubmit={() => {}}>
        <form data-testid="form">
          <Checkbox name="myCheckbox">My Checkbox</Checkbox>
        </form>
      </Formik>
    </TestThemeProvider>,
  )

  // default UI
  expect(getByText('My Checkbox')).toBeTruthy()
  expect(getByLabelText('My Checkbox')).toBeTruthy()
})
