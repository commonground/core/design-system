// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

const getSvg = (fillColor) =>
  `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" fill="${fillColor}"><polygon points="7.243 13.485 3 9.242 4.414 7.828 7.243 10.657 12.899 5 14.314 6.414"></polygon></svg>`

export const StyledInput = styled.input`
  appearance: none;
  width: 1rem;
  height: 1rem;
  vertical-align: middle;
  cursor: ${(p) => (p.disabled ? 'auto' : 'pointer')};

  :not(:checked) {
    border: 1px solid
      ${(p) =>
        p.disabled
          ? p.theme.colorBackgroundChoiceDisabled
          : p.theme.colorBorderChoice};
  }

  :focus {
    border: 1px solid ${(p) => p.theme.colorBorderChoiceFocus};
    outline: 1px solid ${(p) => p.theme.colorBorderChoiceFocus};
  }

  :checked {
    background-color: ${(p) =>
      p.disabled
        ? p.theme.colorBackgroundChoiceDisabled
        : p.theme.colorBackgroundChoiceSelected};
    background-image: url(data:image/svg+xml;base64,${(p) =>
      btoa(getSvg(p.theme.tokens.colorBackground))});
  }
`
