// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Formik, Form } from 'formik'

import { Fieldset, Label, FieldLabel, Legend } from './index'

const formStory = {
  title: 'Components/Form',
  parameters: {
    componentSubtitle: 'Form components using Formik.',
  },
}

export default formStory

export const intro = () => (
  <>
    <Formik initialValues={{ checkboxChecked: true }} onSubmit={() => {}}>
      <Form>
        <Fieldset>
          <Legend>Text fields</Legend>
        </Fieldset>

        <Fieldset>
          <Legend>Radio buttons</Legend>
        </Fieldset>

        <Fieldset>
          <Legend>Checkboxes</Legend>
        </Fieldset>
      </Form>
    </Formik>
  </>
)

export const label = () => <Label>Label</Label>

export const fieldLabel = () => (
  <FieldLabel label="Main label" small="Extra information" />
)

export const fieldsetWithLegend = () => (
  <Fieldset>
    <Legend>Fieldset with legend</Legend>
  </Fieldset>
)
