// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { bool, oneOf } from 'prop-types'

import hex2rgba from '../../utils/hex2rgba'
import getPrimaryStyles from './primary.style'
import getSecondaryStyles from './secondary.style'
import getDangerStyles from './danger.style'
import getLinkStyles from './link.style'

const INNER_BORDER_WIDTH = '2px'

const getPadding = (p) => {
  const { spacing02, spacing03, spacing04, spacing05 } = p.theme.tokens
  return p.size === 'small'
    ? `${spacing02} ${spacing03}`
    : `calc(${spacing04} - ${INNER_BORDER_WIDTH}) calc(${spacing05} - ${INNER_BORDER_WIDTH})`
}

const Button = styled.button.attrs((p) => {
  // Transform link types to button when disabled to prevent unwanted browser behaviour
  if (p.disabled && ('href' in p || 'to' in p)) {
    return { as: 'button' }
  }
})`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  position: relative;
  padding: ${(p) => getPadding(p)};
  /* Minimum sizing for icon-only buttons */
  min-width: ${(p) =>
    p.size === 'small' ? '2.25rem' : p.theme.tokens.spacing09};
  min-height: ${(p) =>
    p.size === 'small' ? '2.25rem' : p.theme.tokens.spacing09};
  border: ${INNER_BORDER_WIDTH} solid transparent;
  vertical-align: middle;
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  font-weight: ${(p) => p.theme.tokens.fontWeightSemiBold};
  line-height: ${(p) => p.theme.tokens.lineHeightText};
  text-align: center;
  text-decoration: none;
  cursor: ${(p) => (p.disabled ? 'not-allowed' : 'pointer')};
  user-select: none;

  &:after {
    background-color: ${(p) =>
      hex2rgba(p.theme.tokens.colorPaletteGray900, 0.25)};
    content: '';
    position: absolute;
    height: 3px;
    bottom: -${INNER_BORDER_WIDTH};
    left: -${INNER_BORDER_WIDTH};
    right: -${INNER_BORDER_WIDTH};
  }

  &:focus {
    border: ${INNER_BORDER_WIDTH} solid ${(p) => p.theme.tokens.colorBackground};
    outline: ${INNER_BORDER_WIDTH} solid ${(p) => p.theme.tokens.colorFocus};

    &:after {
      display: none;
    }
  }

  ${(p) => {
    switch (p.variant) {
      case 'link':
        return getLinkStyles(p)

      case 'danger':
        return getDangerStyles(p)

      case 'secondary':
        return getSecondaryStyles(p)

      case 'primary':
      default:
        return getPrimaryStyles(p)
    }
  }}
`

Button.propTypes = {
  variant: oneOf(['primary', 'secondary', 'danger', 'link']),
  disabled: bool,
  size: oneOf(['small', 'default']),
}

Button.defaultProps = {
  variant: 'primary',
  disabled: false,
  size: 'default',
}

export default Button
