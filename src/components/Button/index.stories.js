// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Link } from 'react-router-dom'
import { MemoryRouter as Router } from 'react-router'
import styled from 'styled-components'
import Icon from '../Icon'
import IconCheck from './check.svg'
import Button from './index'

const buttonStory = {
  title: 'Components/Button',
  parameters: {
    componentSubtitle:
      'Button component which can be used as button, anchor and React Router Link',
    docs: {
      storyDescription: `
Use \`Button\` for both buttons and anchor elements.

Icon spacing should be part of the icon css. The button supports a max icon height of 24px.
      `,
    },
  },
  component: Button,
}

export default buttonStory

export const allButtons = () => (
  <>
    <Button variant="primary">Primary (default)</Button>{' '}
    <Button variant="secondary">Secondary</Button>{' '}
    <Button variant="danger">Danger</Button>{' '}
    <Button variant="link">Link</Button>
    <br />
    <br />
    <Button size="small">Primary small</Button>{' '}
    <Button variant="secondary" size="small">
      Secondary
    </Button>{' '}
    <Button variant="danger" size="small">
      Danger
    </Button>{' '}
    <Button variant="link" size="small">
      Link
    </Button>
  </>
)

export const withIcons = () => (
  <>
    <p>
      Use the <code>{`<Icon inline />`}</code> component to display SVG-icons at
      the left-hand side of the icon's label.
    </p>
    <Button variant="primary" title="Check">
      <IconCheck />
    </Button>{' '}
    <Button variant="secondary" title="Check">
      <IconCheck />
    </Button>{' '}
    <Button variant="danger">
      <Icon as={IconCheck} inline />
      Danger with Icon
    </Button>{' '}
    <Button variant="link">
      <Icon as={IconCheck} inline />
      Link with Icon
    </Button>
    <br />
    <br />
    <Button size="small" title="Check">
      <IconCheck />
    </Button>{' '}
    <Button variant="secondary" size="small" title="Check">
      <IconCheck />
    </Button>{' '}
    <Button variant="danger" size="small">
      <Icon as={IconCheck} inline /> Danger
    </Button>{' '}
    <Button variant="link" size="small">
      <Icon as={IconCheck} inline /> Small link Icon
    </Button>
  </>
)

export const asOtherComponent = () => {
  const StyledButton = styled(Button)`
    font-variant: small-caps;
  `

  return (
    <Router>
      <section>
        <p>Most common usecase: links that look like buttons</p>
        <p>
          Use these with caution - dictation software users may not be able to
          properly identify these actions, as they can say "show buttons" and
          these won't highlight since they are semantically links, even though
          they may look like buttons.
        </p>
        <p>
          When a link receives the `disabled` prop, we always render a button.
        </p>
      </section>

      <Button as="a" href="https://www.duck.com">
        Go to DuckDuckGo.com
      </Button>
      <br />
      <br />
      <Button as="a" href="/" variant="secondary" disabled>
        Disabled Link
      </Button>
      <br />
      <br />
      <StyledButton as={Link} to="/">
        Button with additional styling as react-router link
      </StyledButton>
    </Router>
  )
}
