// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { css } from 'styled-components'

const style = (p) => css`
  background: none;
  border: none;
  padding: ${p.size === 'small'
      ? p.theme.tokens.spacing03
      : p.theme.tokens.spacing05}
    0;

  &:hover,
  &:focus {
    border: none;
  }

  &:after {
    display: none;
  }

  ${p.disabled
    ? css`
        cursor: not-allowed;
        color: ${p.theme.colorTextButtonLinkDisabled};
      `
    : css`
        color: ${p.theme.colorTextButtonLink};

        &:hover {
          color: ${p.theme.colorTextButtonLinkHover};
        }
      `}
`

export default style
