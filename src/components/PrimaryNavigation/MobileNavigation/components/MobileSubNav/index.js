// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useRef, useEffect } from 'react'
import { arrayOf, shape, elementType, string, func } from 'prop-types'
import {
  Wrapper,
  Header,
  CloseButton,
  List,
  ListItem,
  Link,
  IconChevronRight,
} from './index.styles'

const MobileSubNavigation = ({
  LinkComponent,
  ariaLabel,
  items,
  closeFunction,
}) => {
  const refList = useRef()
  const closeDelayed = () => setTimeout(() => closeFunction(), 200)

  useEffect(() => {
    if (refList.current) refList.current.firstChild.firstChild.focus()
  })

  return (
    <Wrapper role="dialog" aria-label={ariaLabel}>
      <Header>
        <CloseButton onClick={closeFunction} />
      </Header>

      <List ref={refList}>
        {items.map(({ name, ...props }) => (
          <ListItem key={name}>
            <Link as={LinkComponent} onClick={closeDelayed} {...props}>
              {name}
              <IconChevronRight />
            </Link>
          </ListItem>
        ))}
      </List>
    </Wrapper>
  )
}

MobileSubNavigation.propTypes = {
  LinkComponent: elementType.isRequired,
  pathname: string,
  ariaLabel: string.isRequired,
  mobileMoreText: string,
  items: arrayOf(
    shape({
      name: string.isRequired,
      Icon: elementType,
      to: string.isRequired,
    }),
  ).isRequired,
  closeFunction: func,
}

const noop = () => {}

MobileSubNavigation.defaultProps = {
  closeFunction: noop,
}

export default MobileSubNavigation
