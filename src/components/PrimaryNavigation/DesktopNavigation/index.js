// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { arrayOf, elementType, shape, string } from 'prop-types'
import { Wrapper, Container, Nav, List, ListItem } from './index.styles'

const DesktopNavigation = ({ items, LinkComponent, ariaLabel }) => (
  <Wrapper>
    <Container>
      <Nav aria-label={ariaLabel}>
        <List>
          {items.map(({ name, to, Icon, ...otherProps }) => {
            return (
              <ListItem key={to}>
                <LinkComponent to={to} {...otherProps}>
                  {name}
                </LinkComponent>
              </ListItem>
            )
          })}
        </List>
      </Nav>
    </Container>
  </Wrapper>
)

DesktopNavigation.propTypes = {
  LinkComponent: elementType.isRequired,
  items: arrayOf(
    shape({
      name: string.isRequired,
      Icon: elementType,
      to: string.isRequired,
    }),
  ).isRequired,
  ariaLabel: string.isRequired,
}

export default DesktopNavigation
