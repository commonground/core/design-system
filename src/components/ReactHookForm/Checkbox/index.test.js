// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import { render, screen } from '@testing-library/react'
import { FormProvider, useForm } from 'react-hook-form'
import TestThemeProvider from '../../../themes/TestThemeProvider'
import Checkbox from './index'

const FormWithCheckboxComponent = () => {
  const methods = useForm({
    defaultValues: {
      myCheckbox: '',
    },
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Checkbox name="myCheckbox">My Checkbox</Checkbox>
      </form>
    </FormProvider>
  )
}
test('default ui', async () => {
  render(
    <TestThemeProvider>
      <FormWithCheckboxComponent />
    </TestThemeProvider>,
  )

  // default UI
  expect(screen.getByText('My Checkbox')).toBeTruthy()
  expect(screen.getByLabelText('My Checkbox')).toBeTruthy()
})
