// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//
import React from 'react'
import { useForm, FormProvider } from 'react-hook-form'

import { Fieldset, Legend } from '../index'
import Button from '../../Button'
import Checkbox from './index'

const checkboxStory = {
  title: 'Components/ReactHookForm/Checkbox',
  parameters: {
    componentSubtitle:
      'Form component to work with checkboxes. Batteries included (label & input).',
  },
  component: Checkbox,
}

export default checkboxStory

export const Intro = () => {
  const methods = useForm({
    defaultValues: { projects: [], pizza: false },
  })
  return (
    <>
      <FormProvider {...methods}>
        <form
          onSubmit={methods.handleSubmit((values) => {
            alert(JSON.stringify(values))
          })}
        >
          <Fieldset>
            <Legend>Multiple options</Legend>
            <Checkbox name="projects" value="project-a">
              Project A
            </Checkbox>

            <Checkbox name="projects" value="project-b">
              Project B
            </Checkbox>
          </Fieldset>

          <Fieldset>
            <Legend>Single option</Legend>

            <Checkbox name="pizza">Do you like pizza?</Checkbox>
          </Fieldset>

          <Button type="submit">Submit</Button>
        </form>
      </FormProvider>
    </>
  )
}

export const Disabled = () => {
  const methods = useForm({
    defaultValues: { myCheckbox: true },
  })
  return (
    <>
      <FormProvider {...methods}>
        <form onSubmit={() => {}}>
          <Checkbox name="myCheckbox" disabled>
            Are you ok?
          </Checkbox>
        </form>
      </FormProvider>
    </>
  )
}
