// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//
import React from 'react'
import styled from 'styled-components'
import { string } from 'prop-types'

export const Label = styled.label`
  display: block;
  width: max-content;
  margin-bottom: ${(p) => p.theme.tokens.spacing01};
  margin-top: ${(p) => p.theme.tokens.spacing06};
  word-break: keep-all;
  color: ${(p) => p.theme.colorTextInputLabel};
  cursor: pointer;
`

export const Fieldset = styled.fieldset`
  padding: 0 0 3rem 0;
  border: 0 none;
`

export const Legend = styled.legend`
  margin: 0;
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  font-size: ${(p) => p.theme.tokens.fontSizeLarge};
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
`

const BlockSpan = styled.span`
  display: block;
`

export const FieldLabel = ({ label, small }) => (
  <>
    <BlockSpan>{label}</BlockSpan>
    {small && <small>{small}</small>}
  </>
)

FieldLabel.propTypes = {
  label: string.isRequired,
  small: string,
}
