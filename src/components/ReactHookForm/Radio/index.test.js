// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import { render, screen } from '@testing-library/react'
import { FormProvider, useForm } from 'react-hook-form'
import TestThemeProvider from '../../../themes/TestThemeProvider'
import Radio from './index'

const FormWithRadioComponent = () => {
  const methods = useForm({
    defaultValues: {
      option: 'option-a',
    },
  })

  return (
    <FormProvider {...methods}>
      <form data-testid="form" onSubmit={methods.handleSubmit(() => {})}>
        <Radio.Group label="Pick an option">
          <Radio name="option" value="option-a">
            Option A
          </Radio>
          <Radio name="option" value="option-b">
            Option B
          </Radio>
        </Radio.Group>
      </form>
    </FormProvider>
  )
}

test('default ui', async () => {
  render(
    <TestThemeProvider>
      <FormWithRadioComponent />
    </TestThemeProvider>,
  )

  // default UI
  expect(screen.getByText('Pick an option')).toBeTruthy()
  expect(screen.getByLabelText('Option A')).toBeTruthy()
  expect(screen.getByLabelText('Option B')).toBeTruthy()
})
