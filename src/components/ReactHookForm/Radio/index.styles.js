// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Label } from '../../Form/index'

export const StyledLabel = styled(Label)`
  color: ${(p) => p.theme.colorText};
  display: inline-flex;
  align-items: center;
  line-height: 1rem;
  margin-right: ${(p) => p.theme.tokens.spacing08};
  cursor: ${(p) => (p.disabled ? 'auto' : 'pointer')};
  margin-top: ${(p) => p.theme.tokens.spacing03};

  input {
    margin-right: ${(p) => p.theme.tokens.spacing04};
  }
`
