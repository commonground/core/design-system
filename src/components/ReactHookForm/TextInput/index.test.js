// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import { func } from 'prop-types'
import { render, act, fireEvent, screen, waitFor } from '@testing-library/react'
import * as Yup from 'yup'
import { useForm, FormProvider } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import TestThemeProvider from '../../../themes/TestThemeProvider'
import TextInput from './index'

const FormComponent = ({ onSubmitHandler }) => {
  const methods = useForm({
    mode: 'onBlur',
    defaultValues: {
      name: '',
    },
    resolver: yupResolver(
      Yup.object().shape({
        name: Yup.string().required('Required'),
      }),
    ),
  })

  const onSubmit = (formData) => onSubmitHandler(formData)

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)} data-testid="form">
        <TextInput name="name">Name</TextInput>
      </form>
    </FormProvider>
  )
}

FormComponent.propTypes = {
  onSubmitHandler: func,
}

test('feedback when touching the input element', async () => {
  const { getByTestId, getByLabelText } = render(
    <TestThemeProvider>
      <FormComponent />
    </TestThemeProvider>,
  )

  // default UI
  expect(screen.getByText('Name')).toBeTruthy()
  expect(screen.getByLabelText('Name')).toBeTruthy()
  expect(() => screen.getByTestId('error-name')).toThrow()

  // fill-in field with invalid value
  await act(async () => {
    fireEvent.blur(screen.getByLabelText('Name'))
  })

  // assert the conditional fields show validation feedback
  expect(screen.getByTestId('error-name').textContent).toEqual('Required')

  // fill-in required fields
  await act(async () => {
    fireEvent.change(getByLabelText('Name'), {
      target: { value: 'My name' },
    })
  })

  // error message is hidden
  expect(() => getByTestId('error-name'))
})

test('feedback when submitting the form', async () => {
  const onSubmitHandlerSpy = jest.fn()

  const { getByTestId, getByLabelText } = render(
    <TestThemeProvider>
      <FormComponent onSubmitHandler={onSubmitHandlerSpy} />
    </TestThemeProvider>,
  )

  // default UI
  expect(screen.getByText('Name')).toBeTruthy()
  expect(screen.getByLabelText('Name')).toBeTruthy()
  expect(screen.queryByTestId('error-name')).not.toBeInTheDocument()

  // submit invalid form
  await act(async () => {
    fireEvent.submit(getByTestId('form'))
  })

  // assert the conditional fields show validation feedback
  expect(getByTestId('error-name')).toHaveTextContent('Required')

  expect(onSubmitHandlerSpy).not.toHaveBeenCalled()

  // fill-in required fields
  await act(async () => {
    fireEvent.change(getByLabelText('Name'), {
      target: { value: 'My name' },
    })
  })

  // error message is hidden
  expect(() => getByTestId('error-name')).toThrow()

  // submit valid form
  await act(async () => {
    fireEvent.submit(getByTestId('form'))
  })

  await waitFor(() =>
    expect(onSubmitHandlerSpy).toHaveBeenCalledWith({
      name: 'My name',
    }),
  )
})
