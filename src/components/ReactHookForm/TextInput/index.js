// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import { bool, oneOf, oneOfType, string, shape, func } from 'prop-types'
import { useFormState, useFormContext, Controller } from 'react-hook-form'
import ErrorMessage from '../../Form/ErrorMessage'
import { FieldLabel } from '../../Form/index'
import Input from '../../Form/TextInput/Input'
import TextArea from '../../Form/TextInput/TextArea'
import { StyledLabel, StyledField } from './index.styles'

const TextInput = ({ type, children, showError, ...props }) => {
  const { control } = useFormContext()
  const { errors } = useFormState()
  const { disabled, name } = props

  const error = errors[`${name}`]

  const hasError = typeof showError === 'undefined' ? error?.message : showError

  return (
    <>
      <StyledLabel disabled={disabled} size={props.size}>
        {children}

        <Controller
          name={name}
          control={control}
          render={({ field }) => {
            return (
              <StyledField
                type={type}
                as={type === 'textarea' ? TextArea : Input}
                {...props}
                {...field}
                className={hasError ? 'invalid' : null}
              />
            )
          }}
        />
      </StyledLabel>
      {hasError && (
        <ErrorMessage data-testid={`error-${name}`}>
          {error?.message}
        </ErrorMessage>
      )}
    </>
  )
}

TextInput.propTypes = {
  children: oneOfType([string, shape({ type: oneOf([FieldLabel]) })])
    .isRequired,
  disabled: bool,
  handleIconAtEnd: func,
  name: string.isRequired,
  showError: bool,
  size: oneOf(['xs', 's', 'm', 'l', 'xl', 'fullWidth']),
  type: oneOf([
    'text',
    'textarea',
    'number',
    'email',
    'password',
    'tel',
    'url',
    'date',
  ]),
}

TextInput.defaultProps = {
  disabled: false,
  size: 'm',
  type: 'text',
}

export default TextInput
