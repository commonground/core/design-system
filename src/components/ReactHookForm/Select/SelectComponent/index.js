// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//
import React, { forwardRef } from 'react'
import {
  arrayOf,
  bool,
  object,
  oneOf,
  oneOfType,
  shape,
  string,
} from 'prop-types'
import StyledReactSelect from './index.styles'

/* eslint-disable react/prop-types */
/* prop-types not supported for forwardRef components */
export const SelectWithRef = (props, ref) => (
  <StyledReactSelect
    ref={ref}
    classNamePrefix="ReactSelect"
    isDisabled={props.disabled}
    {...props}
  />
)
/* eslint-enable react/prop-types */

const SelectComponent = forwardRef(SelectWithRef)

SelectComponent.propTypes = {
  options: arrayOf(
    shape({
      value: oneOfType([string, object]),
      label: string,
    }),
  ).isRequired,
  size: oneOf(['xs', 's', 'm', 'l', 'xl']),
  disabled: bool,
  isMulti: bool,
}

SelectComponent.defaultProps = {
  size: 'm',
  disabled: false,
  isMulti: false,
}

export default SelectComponent
