// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import selectEvent from 'react-select-event'
import { FormProvider, useForm } from 'react-hook-form'
import TestThemeProvider from '../../../themes/TestThemeProvider'
import { Select } from './index'

const SelectComponent = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const methods = useForm({
    defaultValues: {
      selection: options[0],
    },
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="selection" />
        <span data-testid="parsed-selection">
          {methods.watch('selection').value}
        </span>
      </form>
    </FormProvider>
  )
}

test('Select should accept strings as values', async () => {
  const { getByTestId, getByRole } = render(
    <TestThemeProvider>
      <SelectComponent />
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('one')

  await selectEvent.select(getByRole('combobox'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('two')
})

const ObjectSelectComponent = () => {
  const options = [
    { value: { english: 'one' }, label: 'Uno' },
    { value: { english: 'two' }, label: 'Dos' },
    { value: { english: 'three' }, label: 'Tres' },
  ]

  const methods = useForm({
    defaultValues: {
      selection: options[0],
    },
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="selection" />
        <span data-testid="parsed-selection">
          {methods.watch('selection').value.english}
        </span>
      </form>
    </FormProvider>
  )
}

test('Select should accept object as values', async () => {
  const { getByTestId, getByRole } = render(
    <TestThemeProvider>
      <ObjectSelectComponent />
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('one')

  await selectEvent.select(getByRole('combobox'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('two')
})

const MultiSelectComponent = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const methods = useForm({
    defaultValues: {
      selection: [options[0]],
    },
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="selection" isMulti />
        <span data-testid="parsed-selection">
          {methods
            .watch('selection')
            .map((object) => object.value)
            .join(',')}
        </span>
      </form>
    </FormProvider>
  )
}

test('Select should accept strings as values while accepting multiple values', async () => {
  const { getByTestId, getByRole } = render(
    <TestThemeProvider>
      <MultiSelectComponent />
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('one')

  await selectEvent.select(getByRole('combobox'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('one,two')
})

const ObjectMultiSelectComponent = () => {
  const options = [
    { value: { english: 'one' }, label: 'Uno' },
    { value: { english: 'two' }, label: 'Dos' },
    { value: { english: 'three' }, label: 'Tres' },
  ]

  const methods = useForm({
    defaultValues: {
      selection: [options[0]],
    },
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="selection" isMulti />
        <span data-testid="parsed-selection">
          {methods
            .watch('selection')
            .map((object) => object.value.english)
            .join(',')}
        </span>
      </form>
    </FormProvider>
  )
}

test('Select should accept objects as values while accepting multiple values', async () => {
  const { getByTestId, getByRole } = render(
    <TestThemeProvider>
      <ObjectMultiSelectComponent />
    </TestThemeProvider>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('one')

  await selectEvent.select(getByRole('combobox'), 'Dos')

  expect(getByTestId('parsed-selection')).toHaveTextContent('one,two')
})
