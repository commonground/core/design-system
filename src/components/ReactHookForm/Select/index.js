// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import { bool, oneOf, oneOfType, string, shape } from 'prop-types'
import { useFormContext, Controller, useFormState } from 'react-hook-form'
import { FieldLabel } from '../../Form/index'
import { StyledLabel } from '../TextInput/index.styles'
import ErrorMessage from '../../Form/ErrorMessage'
import SelectComponent from './SelectComponent'
import SelectReactHookForm from './SelectReactHookForm'

const Select = ({ children, showError, ...props }) => {
  const { control } = useFormContext()
  const { errors } = useFormState()
  const { disabled, name } = props

  const error = errors[`${name}`]

  const hasError = typeof showError === 'undefined' ? error?.message : showError

  return (
    <>
      <StyledLabel disabled={disabled} />
      {children}

      <Controller
        name={name}
        control={control}
        render={({ field }) => {
          return (
            <SelectReactHookForm
              {...props}
              {...field}
              classNamePrefix="ReactSelect"
              isDisabled={disabled}
            />
          )
        }}
      />
      {hasError && (
        <ErrorMessage data-testid={`error-${name}`}>
          {error?.message}
        </ErrorMessage>
      )}
    </>
  )
}

Select.propTypes = {
  showError: bool,
  children: oneOfType([string, shape({ type: oneOf([FieldLabel]) })]),
  name: string.isRequired,
  size: oneOf(['xs', 's', 'm', 'l', 'xl']),
  disabled: bool,
  isMulti: bool,
}

Select.defaultProps = {
  size: 'm',
  disabled: false,
  isMulti: false,
}

export { Select, SelectReactHookForm, SelectComponent }
