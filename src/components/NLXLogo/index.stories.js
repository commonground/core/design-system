// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import NLXLogo from './index'

const nlxLogoStory = {
  title: 'Components/NLXLogo',
  parameters: {
    componentSubtitle: 'The NLX logo.',
  },
  component: NLXLogo,
}

export default nlxLogoStory

export const theLogo = () => <NLXLogo style={{ height: '50px' }} />

export const onDark = () => (
  <div style={{ backgroundColor: '#000000' }}>
    <NLXLogo onDark style={{ height: '50px' }} />
  </div>
)
