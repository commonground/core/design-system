// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState, useEffect, useCallback } from 'react'
import { node, bool, func, shape, string } from 'prop-types'
import ReactCollapse from 'react-collapse'
import {
  Wrapper,
  CollapsibleButton,
  CollapsibleChevron,
  CollapsibleTitle,
} from './index.styles'

const createRandomId = () => `r${Math.random().toString(36).slice(8)}`

const Collapsible = ({
  title,
  initiallyOpen,
  animate,
  onToggle,
  children,
  buttonLabels,
}) => {
  const [isOpen, setIsOpen] = useState(initiallyOpen)
  const [id, setId] = useState()
  const toggle = useCallback(() => {
    setIsOpen(!isOpen)
    onToggle(!isOpen)
  }, [setIsOpen, isOpen, onToggle])

  useEffect(() => {
    setId(createRandomId())
  }, [])

  return (
    <Wrapper animate={animate} data-testid="collapsible">
      <CollapsibleButton
        aria-haspopup="true"
        aria-expanded={isOpen}
        aria-controls={id}
        onClick={toggle}
      >
        <CollapsibleTitle>{title}</CollapsibleTitle>
        <div>
          <CollapsibleChevron
            animate={animate}
            title={`${
              isOpen ? buttonLabels.open : buttonLabels.close
            } ${title}`}
            flipHorizontal={isOpen}
          />
        </div>
      </CollapsibleButton>

      <ReactCollapse isOpened={isOpen}>
        {/* ReactCollapse does not pass the id-prop */}
        <div id={id}>{children}</div>
      </ReactCollapse>
    </Wrapper>
  )
}

Collapsible.propTypes = {
  title: node.isRequired,
  initiallyOpen: bool,
  animate: bool,
  onToggle: func,
  children: node.isRequired,
  buttonLabels: shape({
    open: string,
    close: string,
  }),
}

Collapsible.defaultProps = {
  initiallyOpen: false,
  animate: true,
  onToggle: (isOpen) => {},
  buttonLabels: {
    open: 'Open',
    close: 'Sluit',
  },
}

export default Collapsible
