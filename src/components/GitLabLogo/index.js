// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'

/**
 * Reusable logo which can be used across the Common Ground projects.
 */
const GitLabLogo = ({ ...props }) => (
  <svg viewBox="0 0 26 24" xmlns="http://www.w3.org/2000/svg" {...props}>
    <g fillRule="nonzero" fill="none">
      <path
        d="M25.962 13.726l-1.454-4.474-2.88-8.866a.495.495 0 0 0-.942 0l-2.881 8.866H8.238L5.358.386a.495.495 0 0 0-.943 0l-2.88 8.866L.08 13.726a.99.99 0 0 0 .36 1.107l12.58 9.141 12.581-9.14a.99.99 0 0 0 .36-1.108"
        fill="#FC6D26"
      />
      <path fill="#E24329" d="M13.021 23.974l4.784-14.722H8.238z" />
      <path fill="#FC6D26" d="M13.021 23.974L8.238 9.252H1.534z" />
      <path
        d="M1.534 9.252L.081 13.726a.99.99 0 0 0 .36 1.107l12.58 9.14L1.534 9.254z"
        fill="#FCA326"
      />
      <path
        d="M1.534 9.252h6.704L5.358.386a.495.495 0 0 0-.943 0l-2.88 8.866z"
        fill="#E24329"
      />
      <path fill="#FC6D26" d="M13.021 23.974l4.784-14.722h6.704z" />
      <path
        d="M24.509 9.252l1.453 4.474a.99.99 0 0 1-.36 1.107l-12.58 9.14 11.487-14.72z"
        fill="#FCA326"
      />
      <path
        d="M24.509 9.252h-6.704l2.88-8.866a.495.495 0 0 1 .943 0l2.88 8.866z"
        fill="#E24329"
      />
    </g>
  </svg>
)

export default GitLabLogo
