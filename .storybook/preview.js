import React from 'react'
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport'
import { GlobalStyles } from '../src'
import {ThemeProvider} from "styled-components";
import {defaultTheme} from '../src/themes'

// Reverse order of dependent decorators
export const decorators = [
  (Story) => (
      <ThemeProvider theme={defaultTheme}>
          <GlobalStyles/>
          <Story/>
      </ThemeProvider>
  ),
]

export const parameters = {
  viewport: {
    viewports: INITIAL_VIEWPORTS,
  },
}
