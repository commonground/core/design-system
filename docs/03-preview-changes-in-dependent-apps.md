Preview changes in dependent applications
=========================================

When you are making changes to the design system, you might want to see the changes reflected in the dependent applications.
For example, if you update the color of the `Button` component, you might want to verify the change in the `txlog-ui` application before pushing your work.
gst
The following steps outline how you can do this using `txlog-ui` as an example dependant application.

    cd ~/projects/design-system                                                   # go into the package directory
    npm pack                                                                      # generate the dist files       
    cd ~/projects/nlx/txlog-ui                                                    # go into the txlog-ui package directory.
    npm install ~/projects/design-system/commonground-design-system-<version>.tgz # link-install the package

Once you start the development task for the `txlog-ui` application (`npm start`),
the changes made in the `design-system` should now be reflected in the `txlog-ui` application.
